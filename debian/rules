#!/usr/bin/make -f
SHELL = /bin/sh -e

# in order to keep output non-intermixed together, disable parallel building
# of different targets in this d/rules but allow running parallel submakes
.NOTPARALLEL:

# get DEB_VERSION
include /usr/share/dpkg/pkg-info.mk
# get DEB_HOST_ARCH DEB_HOST_ARCH_OS DEB_HOST_GNU_TYPE DEB_HOST_MULTIARCH DEB_BUILD_GNU_TYPE
include /usr/share/dpkg/architecture.mk
# get CFLAGS LDFLAGS etc
include /usr/share/dpkg/buildflags.mk

libdir = /usr/lib/${DEB_HOST_MULTIARCH}

ifeq ($(shell dpkg-vendor --derives-from Ubuntu && echo yes),yes)
VENDOR := UBUNTU
DEB_BUILD_PARALLEL = yes
else
VENDOR := DEBIAN
endif

# support parallel build using DEB_BUILD_OPTIONS=parallel=N
ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  MAKEFLAGS += -j$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
endif

# verbose build
V ?= 1

# list of packages we're supposed to build
BUILD_PACKAGES = $(call dpkg_late_eval,BUILD_PACKAGES,dh_listpackages)

enable_system = $(if $(filter qemu-system,${BUILD_PACKAGES}),enable,disable)
enable_linux_user = $(if $(filter qemu-user,${BUILD_PACKAGES}),enable,disable)

FIRMWAREPATH = /usr/share/qemu:/usr/share/seabios:/usr/lib/ipxe/qemu
PKGVERSION = Debian ${DEB_VERSION}
SAVEMODDIR = /run/qemu/$(shell echo -n "${PKGVERSION}" | tr --complement '[:alnum:]+-.~' '_')
sysdatadir = debian/qemu-system-data/usr/share/qemu

ALPHAEV67_CROSSPFX = alpha-linux-gnu-
PPC64_CROSSPFX = powerpc64-linux-gnu-

# we add another set of configure options from debian/control
common_configure_opts = \
	--with-pkgversion="$(PKGVERSION)" \
	--extra-cflags="$(CFLAGS) $(CPPFLAGS)" --extra-ldflags="$(LDFLAGS) -Wl,--as-needed" \
	--prefix=/usr \
	--sysconfdir=/etc \
	--libdir=${libdir} \
	--libexecdir=/usr/lib/qemu \
	--firmwarepath=${FIRMWAREPATH} \
	--localstatedir=/var \
	--disable-blobs \
	--disable-strip \
	--interp-prefix=/etc/qemu-binfmt/%M \
	--localstatedir=/var \
	--with-git-submodules=ignore \

# Cross compiling support
ifneq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
common_configure_opts  += --cross-prefix=$(DEB_HOST_GNU_TYPE)-
endif

ifneq ($(filter $(DEB_TARGET_ARCH), alpha ia64 hppa m68k sh4 sparc), )
# allow configure to run on unsupported arches to build qemu-utils and the like
common_configure_opts += --enable-tcg-interpreter --enable-tools
endif

ifeq (${enable_system},enable)

# list of system (softmmu) targets, from ./configure
system_targets = \
 i386 x86_64 alpha aarch64 arm avr cris hppa m68k microblaze microblazeel \
 mips mipsel mips64 mips64el moxie nios2 or1k ppc ppc64 riscv32 riscv64 rx \
 sh4 sh4eb sparc sparc64 s390x tricore xtensa xtensaeb

# qemu-system subpackages, from d/control
sys_systems = arm mips ppc sparc x86 $(if $(filter ${VENDOR},UBUNTU),s390x,)
systems = ${sys_systems} misc
sysarch_arm   = $(filter aarch64 arm,${system_targets})
sysarch_mips  = $(filter mips mipsel mips64 mips64el,${system_targets})
sysarch_ppc   = $(filter ppc ppc64,${system_targets})
sysarch_sparc = $(filter sparc sparc64,${system_targets})
sysarch_x86   = $(filter i386 x86_64,${system_targets})
sysarch_s390x = $(filter s390x,${system_targets})
sysarch_misc  = $(filter-out $(foreach s,${sys_systems},${sysarch_$s}),${system_targets})
sysarch_x86-microvm   = $(filter x86_64,${system_targets})

else

systems =

endif # enable_system

ifeq (${enable_linux_user},enable)

# list of linux-user targets, from ./configure
user_targets = \
 i386 x86_64 alpha aarch64 aarch64_be arm armeb cris hppa m68k microblaze microblazeel \
 mips mipsel mips64 mips64el mipsn32 mipsn32el nios2 or1k \
 ppc ppc64 ppc64le riscv32 riscv64 sh4 sh4eb sparc sparc64 sparc32plus \
 s390x xtensa xtensaeb

endif	# enable_linux_user

b/configure-stamp: configure
	dh_testdir

	# system build
	rm -rf b/qemu; mkdir -p b/qemu
	cd b/qemu && \
	    ../../configure ${common_configure_opts} --disable-user \
		--${enable_system}-system \
		--${enable_linux_user}-linux-user \
		--enable-modules \
		--enable-module-upgrades \
		$(shell sh debian/extract-config-opts \
		    $(DEB_HOST_ARCH_OS)-$(DEB_HOST_ARCH) debian/control) \
		$(QEMU_CONFIGURE_OPTIONS) || \
	 { echo ===== CONFIGURE FAILED ===; tail -n 50 config.log; exit 1; }

ifneq ($(filter $(DEB_HOST_ARCH),amd64),)
	# microvm system
	rm -rf b/qemu-microvm; mkdir -p b/qemu-microvm
	cd b/qemu-microvm && \
		../../configure ${common_configure_opts} --disable-user \
		--enable-system --enable-kvm \
		--disable-linux-user --disable-modules --disable-docs \
		--disable-libssh --disable-tcmalloc --disable-glusterfs \
		--disable-seccomp --disable-bzip2 --disable-slirp --disable-vde \
		--disable-netmap --disable-hax --disable-hvf --disable-whpx \
		--disable-cocoa --disable-lzfse \
		--disable-guest-agent --disable-guest-agent-msi --disable-tools \
		--disable-gnutls --disable-gcrypt --disable-auth-pam \
		--disable-replication --disable-bochs --disable-cloop \
		--disable-dmg --disable-parallels --disable-qed --disable-vdi \
		--disable-sheepdog --disable-capstone --disable-libpmem --disable-plugins \
		--disable-snappy --disable-lzo --disable-usb-redir \
		--disable-libusb --disable-smartcard --disable-libnfs  \
		--disable-libiscsi --disable-rbd  --disable-spice --disable-attr \
		--disable-cap-ng --disable-linux-aio --disable-brlapi \
		--disable-virglrenderer --disable-vnc-jpeg --disable-vnc-sasl \
		--disable-vnc-png --disable-rdma --disable-pvrdma \
		--disable-fdt --disable-curl --disable-curses --disable-sdl \
		--disable-gtk  --disable-tpm --disable-vte --disable-vnc  \
		--disable-xen --disable-opengl --target-list=x86_64-softmmu \
		--audio-drv-list="" --disable-xfsctl --disable-vvfat \
		--disable-iconv --disable-nettle \
		--disable-tcg-interpreter --disable-tcg \
		--without-default-devices \
		$(QEMU_CONFIGURE_OPTIONS) || \
	 { echo ===== CONFIGURE FAILED ===; tail -n 50 config.log; exit 1; }
	echo "#define CONFIG_MICROVM_DEFAULT 1" >> b/qemu-microvm/x86_64-softmmu/config-target.h
endif

ifeq ($(enable_linux_user),enable)
# do not use debian/configure-opts here, all optional stuff will be enabled
# automatically, dependencies are already verified in the main build
# by default this would detect linker option --static-pie, but that
# breaks some use cases of qemu-static builds (LP: #1908331), therefore
# add --disable-pie to get "real static" builds.
	rm -rf b/user-static; mkdir b/user-static
	cd b/user-static && \
	    ../../configure ${common_configure_opts} \
		--static --disable-pie --disable-system \
		--target-list="$(addsuffix -linux-user,${user_targets})"
endif
	touch $@

build-arch: b/build-stamp
b/build-stamp: b/configure-stamp
	dh_testdir

	# system and utils build
	$(MAKE) -C b/qemu V=${V}
ifeq (${enable_system},enable)
	rm -f b/qemu/pc-bios/bamboo.dtb b/qemu/pc-bios/canyonlands.dtb
	dtc -o b/qemu/pc-bios/bamboo.dtb pc-bios/bamboo.dts
	dtc -o b/qemu/pc-bios/canyonlands.dtb pc-bios/canyonlands.dts
endif

ifneq ($(filter $(DEB_HOST_ARCH),amd64),)
	# microvm system
	$(MAKE) -C b/qemu-microvm V=${V}
endif

ifeq ($(enable_linux_user),enable)
	# user-static build
	# we use this invocation to build just the binaries
	$(MAKE) -C b/user-static V=${V} $(addprefix qemu-,${user_targets})
	sed -e 's/qemu\\-user\\-static/qemu\\-user/g' \
	    -e 's/ (static version)//' \
	    debian/qemu-user-static.1 > debian/qemu-user.1
endif
	touch $@

define inst-system
	mkdir -p debian/qemu-system-$1/usr/share/man/man1 debian/qemu-system-$1/usr/bin
	for t in ${sysarch_$1}; do \
	    mv debian/tmp/usr/bin/qemu-system-$$t debian/qemu-system-$1/usr/bin/qemu-system-$$t; \
	    echo ".so man1/qemu-system.1" > debian/qemu-system-$1/usr/share/man/man1/qemu-system-$$t.1; \
	done
	echo sysarch:$1=\
$(if $(wordlist 10,20,${sysarch_$1}),\
$(wordlist 1,8,${sysarch_$1})\$${Newline}   $(wordlist 9,20,${sysarch_$1}),\
${sysarch_$1}) \
> debian/qemu-system-$1.substvars
	echo sysprovides:$1=${addprefix qemu-system-,${filter-out $1,${sysarch_$1}}} | \
	  sed -e 's/ /, /g' -e 'y/_/-/' >> debian/qemu-system-$1.substvars
	dh_link -pqemu-system-$1 usr/share/doc/qemu-system-common usr/share/doc/qemu-system-$1/common

endef
# inst-kvm-link package binary-suffix
define inst-kvm-link
	dh_link -p $1 usr/bin/qemu-system-$2 usr/bin/kvm
	mkdir -p debian/$1/usr/share/man/man1
	sed 's/@ARCH@/$2/g' debian/kvm.1 > debian/$1/usr/share/man/man1/kvm.1
	touch -r debian/kvm.1 debian/$1/usr/share/man/man1/kvm.1
endef

binary-arch:
	dh_testdir
	dh_testroot
	dh_prep -a
	dh_installdirs -a

	# system and utils install
	$(MAKE) -C b/qemu DESTDIR=$(CURDIR)/debian/tmp install \
	  KEYMAPS= ICON_SIZES=

# save block-extra loadable modules on upgrades
# other module types for now (5.0) can't be loaded at runtime, only at startup
	echo 'case $$1 in (upgrade|deconfigure) mkdir -p ${SAVEMODDIR}; cp -p ${libdir}/qemu/block-*.so ${SAVEMODDIR}/;; esac' \
	  >> debian/qemu-block-extra.prerm.debhelper
	echo 'case $$1 in (purge|remove) rm -f ${SAVEMODDIR}/block-*.so;; esac' \
	  >> debian/qemu-block-extra.postrm.debhelper

ifeq (${enable_system},enable)

	# qemu-system subpackages
	mv debian/tmp/usr/share/man/man1/qemu.1 debian/tmp/usr/share/man/man1/qemu-system.1
	$(foreach s,${systems},$(call inst-system,$s))

ifneq ($(filter $(DEB_HOST_ARCH),amd64),)
	# microvm system
	cp b/qemu-microvm/x86_64-softmmu/qemu-system-x86_64 debian/qemu-system-x86/usr/bin/qemu-system-x86_64-microvm
	echo ".so man1/qemu-system.1" > debian/qemu-system-x86/usr/share/man/man1/qemu-system-x86_64-microvm.1
endif

ifeq ($(DEB_HOST_ARCH_OS),linux)

# /usr/bin/kvm handy link multi-arch from old qemu-kvm package
# on i386, should we link to qemu-system-i386? how about x32?
ifneq ($(filter ${DEB_HOST_ARCH},amd64 i386),)
	$(call inst-kvm-link,qemu-system-x86,x86_64)
ifeq (${VENDOR},UBUNTU)
# on ubuntu *-spice existed, may be used in libvirt xml and scripts - keep links for compatibility
	sed 's/@KVM@/ -enable-kvm/' debian/kvm-spice > debian/qemu-system-x86/usr/bin/kvm-spice
	sed 's/@KVM@//' debian/kvm-spice > debian/qemu-system-x86/usr/bin/qemu-system-x86_64-spice
	touch -r debian/kvm-spice debian/qemu-system-x86/usr/bin/kvm-spice debian/qemu-system-x86/usr/bin/qemu-system-x86_64-spice
	chmod 0755                debian/qemu-system-x86/usr/bin/kvm-spice debian/qemu-system-x86/usr/bin/qemu-system-x86_64-spice
	install -p -t debian/qemu-system-x86/usr/share/man/man1/ debian/kvm-spice.1
	echo ".so man1/kvm-spice.1" > debian/qemu-system-x86/usr/share/man/man1/qemu-system-x86_64-spice.1
endif
endif
ifneq ($(filter ${DEB_HOST_ARCH},arm64),)
	$(call inst-kvm-link,qemu-system-arm,aarch64)
endif
ifneq ($(filter ${DEB_HOST_ARCH},armhf armel),)
	$(call inst-kvm-link,qemu-system-arm,arm)
endif
ifneq ($(filter ${DEB_HOST_ARCH},ppc64 ppc64el),)
	$(call inst-kvm-link,qemu-system-ppc,ppc64)
endif
ifneq ($(filter ${DEB_HOST_ARCH},s390x),)
	$(call inst-kvm-link,qemu-system-$(if $(filter ${VENDOR},UBUNTU),s390x,misc),s390x)
endif
ifeq ($(VENDOR),UBUNTU)
# apport hook is ubuntu-specific
	install -p -D -t debian/qemu-system-common/usr/share/apport/package-hooks/ \
		 debian/source_qemu.py
endif

# virtfs-proxy-helper and qemu-bridge-helper are linux-specific
	for f in usr/lib/qemu/virtfs-proxy-helper \
	         usr/share/man/man1/virtfs-proxy-helper.1 \
		 usr/lib/qemu/qemu-bridge-helper \
	; do \
	  mkdir -p debian/qemu-system-common/$${f%/*} ; \
	  mv debian/tmp/$$f debian/qemu-system-common/$$f ; \
	done

ifneq (${DEB_HOST_ARCH},sparc64)
# virtiofsd needs libseccomp which is not ported to sparc (not even the kernel part),
# so install it only on non-sparc
	for f in usr/lib/qemu/virtiofsd \
		 usr/share/man/man1/virtiofsd.1 \
		 usr/share/qemu/vhost-user/50-qemu-virtiofsd.json \
	; do \
	  mkdir -p debian/qemu-system-common/$${f%/*} ; \
	  mv debian/tmp/$$f debian/qemu-system-common/$$f ; \
	done
endif

## spice is optional (see d/control)
## hw-display-qxl depends on spice
#	mkdir -p debian/qemu-system-common${libdir}/qemu
#	for f in ui-spice-core.so ui-spice-app.so hw-display-qxl.so chardev-spice.so audio-spice.so; do \
#	  [ ! -f debian/tmp${libdir}/qemu/$$f ] || \
#	      mv debian/tmp${libdir}/qemu/$$f \
#	       debian/qemu-system-common${libdir}/qemu/ ; \
#	done
#
## ui-egl-headless is linux-specific
#	[ -f debian/qemu-system-common${libdir}/qemu/ui-egl-headless.so ] || \
#	    mv debian/tmp${libdir}/qemu/ui-egl-headless.so \
#		debian/qemu-system-common${libdir}/qemu/
## alsa is linux-specific
#	[ -f debian/qemu-system-common${libdir}/qemu/audio-alsa.so ] || \
#	    mv debian/tmp${libdir}/qemu/audio-alsa.so \
#		debian/qemu-system-common${libdir}/qemu/

endif # linux

endif # enable_system

ifeq ($(enable_linux_user),enable)
	mkdir -p debian/qemu-user/usr/bin
	mv -t debian/qemu-user/usr/bin $(patsubst %,debian/tmp/usr/bin/qemu-%,${user_targets})
	dh_link -p qemu-user $(patsubst %,usr/share/man/man1/qemu-user.1 usr/share/man/man1/qemu-%.1,${user_targets})

	# qemu-user-static
	mkdir -p debian/qemu-user-static/usr/bin debian/qemu-user-static/usr/share/man/man1
	for t in ${user_targets}; do \
	   cp -p b/user-static/$$t-linux-user/qemu-$$t \
	       debian/qemu-user-static/usr/bin/qemu-$$t-static ; \
	   ln -s qemu-user-static.1 \
	       debian/qemu-user-static/usr/share/man/man1/qemu-$$t-static.1 ; \
	done

	# binfmt support
	./debian/binfmt-install qemu-user-static
	./debian/binfmt-install qemu-user-binfmt
endif	# enable_linux_user

	dh_install -a
	dh_missing --list-missing
	dh_installdocs -a -Nqemu-user-binfmt
	dh_installchangelogs -a -Nqemu-user-binfmt -XChangelog
	dh_installdocs -a -pqemu-user-binfmt --link-doc=qemu-user
	dh_installman -a
	dh_installudev -a
ifeq (${enable_system},enable)
# qemu-ifup is arch-specific
	install -D debian/qemu-ifup.$(DEB_HOST_ARCH_OS) \
		debian/qemu-system-common/etc/qemu-ifup
endif
	dh_installinit -a -pqemu-guest-agent
	dh_installsystemd -a -pqemu-guest-agent --no-start --no-enable
	dh_link -a
	dh_lintian -a
	dh_strip -a
	dh_compress -a
	dh_fixperms -a
	dh_shlibdeps -a
	dh_installdeb -a
ifeq ($(enable_linux_user),enable)
# after shlibdeps finished, grab ${shlibs:Depends} from -user package
# and transform it into Built-Using field for -user-static.
# See also dpkg-query bug #588505
	if [ -f debian/qemu-user.substvars ]; then \
	  pkgs=$$(sed -n -e's/([^)]*)//g' -e's/,//g' -e's/^shlibs:Depends=//p' debian/qemu-user.substvars); \
	  srcs=; for p in $$pkgs; do \
	    srcs="$$srcs $$(dpkg-query -f '$${source:Package} (= $${source:Version}),' -W $$p)"; \
	  done ; \
	  echo "built-using=$$srcs" >> debian/qemu-user-static.substvars ; \
	fi
endif
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

ifneq (,$(wildcard debian/control-in))
# only include rules for debian/control if debian/control-in is present
debian/control: debian/control-in debian/rules
	echo '# autogenerated file, please edit debian/control-in' > $@.tmp
	sed -e 's/^:$(shell echo ${VENDOR} | tr '[A-Z]' '[a-z]')://' \
		-e '/^:[a-z]*:/D' $< >> $@.tmp
	mv -f $@.tmp $@
	chmod -w $@
endif

### openbios rules
b/openbios/config-host.mak:
	mkdir -p b/openbios
	cd b/openbios && ../../roms/openbios/config/scripts/switch-arch builtin-ppc builtin-sparc32 builtin-sparc64
build-openbios: $(addprefix b/openbios/obj-, $(addsuffix /.built, ppc sparc32 sparc64))
b/openbios/obj-%/.built: b/openbios/config-host.mak
	${MAKE} -C ${@D} V=${V} EXTRACFLAGS="-ffreestanding -fno-pic -fno-stack-protector"
	@touch $@
install-openbios: build-openbios
	install -m 0644 b/openbios/obj-ppc/openbios-qemu.elf "${sysdatadir}/openbios-ppc"
	install -m 0644 b/openbios/obj-sparc32/openbios-builtin.elf "${sysdatadir}/openbios-sparc32"
	install -m 0644 b/openbios/obj-sparc64/openbios-builtin.elf "${sysdatadir}/openbios-sparc64"
	install -m 0644 -t "${sysdatadir}" \
		b/openbios/obj-sparc32/QEMU,tcx.bin \
		b/openbios/obj-sparc32/QEMU,cgthree.bin \
		b/openbios/obj-sparc64/QEMU,VGA.bin
sysdata-components += openbios

### powernv firmware in roms/skiboot
build-skiboot: b/skiboot/skiboot.lid
b/skiboot/skiboot.lid:
	mkdir -p b/skiboot
	${MAKE} -C b/skiboot -f ${CURDIR}/roms/skiboot/Makefile \
	  SRC=${CURDIR}/roms/skiboot \
	  CROSS_COMPILE=${PPC64_CROSSPFX} V=${V}
install-skiboot: b/skiboot/skiboot.lid
	install -m 0644 -t "${sysdatadir}" $<
sysdata-components += skiboot

### x86 optionrom
build-x86-optionrom: b/optionrom/built
b/optionrom/built:
	mkdir -p b/optionrom
	${MAKE} -f ${CURDIR}/debian/optionrom.mak -C b/optionrom SRC_PATH="${CURDIR}" all
	touch $@
install-x86-optionrom: build-x86-optionrom b/install-indep-prep.stamp | ${sysdatadir}
	${MAKE} -f ${CURDIR}/debian/optionrom.mak -C b/optionrom SRC_PATH="${CURDIR}" install DESTDIR="${CURDIR}/${sysdatadir}"
sysdata-components += x86-optionrom

### sgabios.
# The Makefile is too complex and forces current date to be embedded to binary
build-sgabios: b/sgabios/sgabios.bin
b/sgabios/sgabios.bin:
	mkdir -p b/sgabios
	cc -c -o b/sgabios/sgabios.o roms/sgabios/sgabios.S -Wall -Os -m32 -nostdlib \
	  -DBUILD_DATE="\"$$(LC_ALL=C date -u -r roms/sgabios/sgabios.S)\"" \
	  -DBUILD_SHORT_DATE="\"$$(date -u +%D -r roms/sgabios/sgabios.S)\"" \
	  -DBUILD_HOST=\"debian\" -DBUILD_USER=\"build\"
	ld -T roms/sgabios/rom16.ld -nostdlib b/sgabios/sgabios.o -o b/sgabios/sgabios.elf
	objcopy -O binary b/sgabios/sgabios.elf b/sgabios/sgabios.bin
	cc -Wall -O2 -o b/sgabios/csum8 roms/sgabios/csum8.c
	b/sgabios/csum8 b/sgabios/sgabios.bin
install-sgabios: b/sgabios/sgabios.bin
	install -m 0644 $< ${sysdatadir}/sgabios.bin
sysdata-components += sgabios

### qboot, aka bios-microvm
build-qboot: b/qboot/bios.bin
b/qboot/bios.bin: | b
	rm -rf b/qboot
	meson setup roms/qboot b/qboot
	ninja -C b/qboot
install-qboot: b/qboot/bios.bin
	install -m 0644 $< ${sysdatadir}/qboot.rom
	# 5.0 & 5.1 compat symlink, can go for bullseye final
	ln -s qboot.rom ${sysdatadir}/bios-microvm.bin
sysdata-components += qboot

### alpha firmware in roms/palcode-clipper
build-palcode-clipper: b/qemu-palcode/palcode-clipper
b/qemu-palcode/palcode-clipper: | b
	cp -al roms/qemu-palcode b/
	${MAKE} -C ${CURDIR}/b/qemu-palcode CROSS=${ALPHAEV67_CROSSPFX}
	${ALPHAEV67_CROSSPFX}strip b/qemu-palcode/palcode-clipper
install-palcode-clipper: b/qemu-palcode/palcode-clipper
	install -m 0644 $< ${sysdatadir}/palcode-clipper
sysdata-components += palcode-clipper

### SLOF
build-slof: b/SLOF/boot_rom.bin
b/SLOF/boot_rom.bin:
	cp -al roms/SLOF b/
	env -u LDFLAGS -u CFLAGS $(MAKE) -C b/SLOF qemu CROSS="powerpc64-linux-gnu-" V=${V}
install-slof: b/SLOF/boot_rom.bin
	install -m 0644 $< ${sysdatadir}/slof.bin
sysdata-components += slof

### s390x firmware in pc-bios/s390-ccw
build-s390x-fw: b/s390fw/built
b/s390fw/built:
	mkdir -p b/s390fw
	${MAKE} -f debian/s390fw.mak OUT=b/s390fw/
	touch $@
install-s390x-fw: build-s390x-fw
	install -m 0644 -t "${sysdatadir}" b/s390fw/s390*.img
sysdata-components += s390x-fw

### hppa-firmware (roms/seabios-hppa)
build-hppa-fw: b/hppafw/hppa-firmware.img
b/hppafw/hppa-firmware.img: | b
	mkdir -p b/hppafw
	${MAKE} -C roms/seabios-hppa OUT=../../b/hppafw/ PYTHON=python3 parisc
	hppa-linux-gnu-strip -R.note -R.comment $@
install-hppa-fw: b/hppafw/hppa-firmware.img
	install -m 0644 $< ${sysdatadir}
sysdata-components += hppa-fw

### opensbi (riscv firmware)
# we only build v64 variants, not v32
build-opensbi: b/opensbi/.built
b/opensbi/.built: | b
	mkdir -p b/opensbi
	${MAKE} -C roms/opensbi O=../../b/opensbi CROSS_COMPILE=riscv64-linux-gnu- V=${V} PLATFORM=generic
	riscv64-linux-gnu-strip --strip-unneeded -R.comment -R.note b/opensbi/platform/generic/firmware/fw_dynamic.elf
	touch $@
install-opensbi: build-opensbi
	install -m 0644 b/opensbi/platform/generic/firmware/fw_dynamic.bin ${sysdatadir}/opensbi-riscv64-generic-fw_dynamic.bin
	install -m 0644 b/opensbi/platform/generic/firmware/fw_dynamic.elf ${sysdatadir}/opensbi-riscv64-generic-fw_dynamic.elf
sysdata-components += opensbi

build-indep: $(addprefix build-, ${sysdata-components})

b/install-indep-prep.stamp:
	dh_testdir
	dh_testroot
	dh_prep -i -Xdebian/tmp
	touch $@

${sysdatadir}:
	mkdir -p -m 0755 $@
b:
	mkdir -p $@

.PHONY: $(addprefix build-  , ${sysdata-components})
.PHONY: $(addprefix install-, ${sysdata-components})
$(addprefix build-  , ${sysdata-components}): | b
$(addprefix install-, ${sysdata-components}): b/install-indep-prep.stamp | ${sysdatadir}

binary-indep: b/install-indep-prep.stamp \
	        $(addprefix install-, ${sysdata-components}) \
		| ${sysdatadir}
	dh_install -i
	rm -f debian/qemu-system-data/usr/share/qemu/keymaps/Makefile
	dh_installdocs -i
	dh_installchangelogs -i -XChangelog
	dh_lintian -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

build: build-arch build-indep
binary: binary-arch binary-indep

clean:	debian/control
	dh_testdir
	rm -rf b
	find scripts/ -name '*.pyc' -delete || :
	rm -f debian/qemu-user.1
	dh_clean

.PHONY: build clean binary-arch binary-indep binary build-arch build-indep build

get-orig-source:
	./debian/get-orig-source.sh ${DEB_VERSION}

.PHONY: get-orig-source
